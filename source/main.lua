--load assets
function love.load()
x = 100 
xdir = 1
y = 100
ydir = 1 

speedIncriment = 1
speed = {"speed: ", 150}

cornerhits = {"corner hits: ", 0}
nehits = 0
nwhits = 0
swhits = 0
sehits = 0

logo = love.graphics.newImage("dvd-logo.png")
logoH = logo:getHeight()
logoW = logo:getWidth()

winW = 600
winH = 400
love.window.setMode(winW, winH, {resizable=true, fullscreen=false})
love.window.setTitle("DVD Game")

rgba = {0, 0, 0, 0}
love.graphics.setBackgroundColor(rgba)

comfortaa = love.graphics.newFont("Comfortaa-Regular.ttf")
comfortaaBold = love.graphics.newFont("Comfortaa-Bold.ttf")

end


function love.update(dt)
--[[ dt is delta time, it helps us prevent CPU dependent game speeds 
print(dt) ]]--
	ne = {winW, 0}
	nw = {0, 0}
	sw = {0, winH}
	se = {winW, winH}

	function windowSize() 
		winW, winH = love.graphics.getDimensions()

		if love.keyboard.isDown("f") and not love.window.getFullscreen()  then
			love.window.setMode(600, 400, {resizable=true, fullscreen=true})
		elseif love.keyboard.isDown("w") and love.window.getFullscreen() then
			love.window.setMode(600, 400, {resizable=true, fullscreen=false})
		end
	end

	function changeSpeed()
		if love.keyboard.isDown("up")  and speed[2] < 600 then
			speed[2] = speed[2] + speedIncriment
		elseif love.keyboard.isDown("down") and speed[2] >2 then
			speed[2] = speed[2] - speedIncriment 
		end
			
	end
	
	function checkCornerHit()
		if(x > winW - logoW and y < 0) then 
			cornerhits[2] = cornerhits[2] + 1
			nehits = nehits + 1
		elseif (x < 0 and y < 0) then 
			nwhits = nwhits + 1
			cornerhits[2] = cornerhits[2] + 1
		elseif ( x < 0 and y > winH - logoH) then
			swhits = swhits + 1
			cornerhits[2] = cornerhits[2] + 1
		elseif (x > winW - logoW and y > winH - logoH) then
			sehits = sehits + 1
			cornerhits[2] = cornerhits[2] + 1
		end
	end

	function colorBg()
		if(nehits > 255) then nehits = 0 end
		if(nwhits > 255) then nwhits = 0 end
		if(swhits > 255) then swhits = 0 end
		if(sehits > 255) then sehits = 0 end
		rgba[1] = nehits / 255
		rgba[2] = nwhits / 255
		rgba[3] = swhits / 255
		rgba[4] = sehits / 255
	end
		

	function moveLogo()
		if(x > winW - logoW) then 
			xdir = 0
		elseif(x < 0) then
			xdir = 1
		end
		
		if(xdir == 1 and x < winW - logoW) then
			x = x + speed[2] * dt
		elseif(xdir == 0 and x > 0) then
			x = x - speed[2] * dt
		end
		
		if(y > winH - logoH) then 
			ydir = 0
		elseif(y <  0) then
			ydir = 1
		end
		
		if(ydir == 1 and y < winH - logoH ) then
			y = y + speed[2] * dt
		elseif(ydir == 0 and y > 0) then
			y = y - speed[2] * dt
		end
	end
	
	checkCornerHit()
	colorBg()
	moveLogo()
	windowSize()
	changeSpeed()
		

end
function love.draw()
	love.graphics.draw(logo, x, y)
	love.graphics.print(cornerhits, comfortaaBold, 0, 0)
	love.graphics.print(speed, comfortaaBold, 0, 30)
	love.graphics.setBackgroundColor(rgba)
end
