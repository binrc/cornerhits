# Windows
Click 'cornerhits.exe' and the game should run. 
If you receive an error you are beyond my help

# Linux
run the following to make the game executable: 
$ chmod u+x cornerhits.sh 
$ chmod u+x love-11.3-x86_64.AppImage 

to start the game, run: 
$ ./cornerhits.sh

If you receive an error you probably don't 
have the dependencies required for running an 
appimage on your system. 

# Controls
Press the up/down arrow keys to speed up/slow down
the bouncing logo.

Resize the window to try to get a corner hit. 

Press the f key  to enable fullscreen mode, press
the w key to enable windowed mode. 

The background color changes based on which corner is hit. 
Can you turn the background into your favorite color?
